﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class sqlConnection    
    {
        /// <summary>
        /// This function invokes a procedure in SQL
        /// </summary>
        /// <param name="procedureName">Please submit the name in full, including added characters.</param>
        /// <returns>DataSet with all the data that accepted from the database.</returns>
        public DataSet callProcedure(string procedureName)
        {
            string connString = "Data Source=SHREIBER;Initial Catalog=ourShapes;Integrated Security=True";

            try
            {
                //sql connection object
                using (SqlConnection cn = new SqlConnection(connString))
                {
                    string spName = procedureName;
                    SqlCommand cmd = new SqlCommand(spName, cn);
                    cn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet("res");
                    da.Fill(ds);
                    cn.Close();
                    return ds;

                }
            }
            catch (Exception ex)
            {
                //display error message
                DataSet ds = new DataSet("res");
                Console.WriteLine(ex.Message);
                return ds;
            }

        }

    }
}
