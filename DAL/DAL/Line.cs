﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    [Implement(typeof(Iline))]
    class Line : Iline
    {
        public DataSet AddLine(List<SqlParameter> list)
        {
            string connString = "Data Source=SHREIBER;Initial Catalog=ourShapes;Integrated Security=True";

            try
            {
                //sql connection object
                using (SqlConnection cn = new SqlConnection(connString))
                {
                    //
                    string spName = @"dbo.[save_line]";
                    SqlCommand cmd = new SqlCommand(spName, cn);
                    cn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet("res");
                    da.Fill(ds);
                    cn.Close();
                    return ds;                  
                }
            }
            catch (Exception ex)
            {
                //display error message
                DataSet ds = new DataSet("res");
                Console.WriteLine(ex.Message);
                return ds;
            }
        }

        public DataSet DeleteLine(List<SqlParameter> list)
        {
            throw new System.NotImplementedException();
        }

        public DataSet GetLines(List<SqlParameter> list)
        {
            throw new System.NotImplementedException();
        }
    }
}
