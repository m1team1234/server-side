﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    [Implement(typeof(Iuser))]
   public class user:Iuser
    {
        public user()
        {
            name = new List<string>()
            {
                "@UserNames",
                "@passoword"
            };
        }
        public List<string> name { get; set; }
        public DataSet GetUser(List<SqlParameter> list)
        {
           
            string connString = "Data Source=SHREIBER;Initial Catalog=ourShapes;Integrated Security=True";

            try
            {
                //sql connection object
                using (SqlConnection cn = new SqlConnection(connString))
                {
                    string spName = @"dbo.[Login_user]";
                    SqlCommand cmd = new SqlCommand(spName, cn);
                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i].ParameterName = name[i];
                        cmd.Parameters.Add(list[i]);
                    }                     
                     cn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet("res");
                    da.Fill(ds);
                    cn.Close();
                    return ds;
                    
                }
            }
            catch (Exception ex)
            {
                //display error message
                DataSet ds = new DataSet("res");
                Console.WriteLine(ex.Message);
                return ds;
            }

        }

        public DataSet Register(List<SqlParameter> list)
        {
            string connString = "Data Source=SHREIBER;Initial Catalog=ourShapes;Integrated Security=True";

            try
            {
                //sql connection object
                using (SqlConnection cn = new SqlConnection(connString))
                {
                    string spName = @"dbo.[insert_new_user]";
                    SqlCommand cmd = new SqlCommand(spName, cn);
                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i].ParameterName=name[i];
                        cmd.Parameters.Add(list[i]);
                    }
                    cn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet("res");
                    da.Fill(ds);
                    cn.Close();
                    return ds;
                   
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                DataSet ds = new DataSet("res");               
                return ds;
            }
        }

        public void DeleteUser(List<SqlParameter> list)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(List<SqlParameter> list)
        {
            throw new NotImplementedException();
        }
    }
}
