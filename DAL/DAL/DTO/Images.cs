﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DTO
{
    class Images
    {
        public int ImageId { get; set; }
        public int UserId { get; set; }
        public string ImageName{ get; set; }
        public string comments{ get; set; }
    }
}
