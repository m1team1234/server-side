﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DTO
{
    class Shapes
    {
        public int ShapesId { get; set; }
        public string ShapesName { get; set; }
    }
}
