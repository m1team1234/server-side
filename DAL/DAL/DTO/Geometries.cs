﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DTO
{
    class Geometries:Drawings
    {
        public int GeometryId { get; set; }
        public float CenterPointX { get; set; }
        public float CenterPointY { get; set; }
        public float WidthX { get; set; }
        public float WidthY { get; set; }
        public int ShapesId { get; set; }
        public int ColorsId { get; set; }
    }
}
