﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DTO
{
    class Editings
    {
        public int EditingId{ get; set; }
        public int ImageId { get; set; }
        public int DrawingId { get; set; }
        public int UserId { get; set; }
        public DateTime EditingDate { get; set; }
    }
}
