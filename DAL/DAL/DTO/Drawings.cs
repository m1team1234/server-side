﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DTO
{
    abstract class Drawings
    {
        public int DrawingId { get; set; }
        public int ImageId { get; set; }
        public int LineColor { get; set; }
        public string DrawingName { get; set; }
        public string Comments { get; set; }
    }
}
