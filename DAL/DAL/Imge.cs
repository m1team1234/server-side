﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    [Implement(typeof(Iimage))]

    public class Imge : Iimage
    {
        public DataSet AddImage(List<SqlParameter> list)
        {
            string connString = "Data Source=SHREIBER;Initial Catalog=ourShapes;Integrated Security=True";

            try
            {
                //sql connection object
                using (SqlConnection cn = new SqlConnection(connString))
                {
                    string spName = @"dbo.[save_image]";
                    SqlCommand cmd = new SqlCommand(spName, cn);
                    for (int i = 0; i < list.Count; i++)
                    {
                        cmd.Parameters.Add(list[i]);
                    }
                    cn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet("res");
                    da.Fill(ds);
                    cn.Close();
                    return ds;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                DataSet ds = new DataSet("res");
                return ds;
            }
        }

        public void DeleteImage(int imageId)
        {
            throw new NotImplementedException();
        }

        public string GetImage(int imageId)
        {
            throw new NotImplementedException();
        }

        public List<DataSet> GetImageDetails(int imageId)
        {
            throw new NotImplementedException();
        }

        public List<DataSet> GetImagesDetailsPerUser(int userId)
        {
            throw new NotImplementedException();
        }
    }
}
