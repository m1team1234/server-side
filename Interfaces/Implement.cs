﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces

{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]

   public class Implement :Attribute
    {
        public Type Implementaion { get; set; }

        public Implement(Type name)
        {
            Implementaion = name;
        }
    }
}
