﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
   public interface Iline
    {
        DataSet AddLine(List<SqlParameter> list);
        DataSet GetLines(List<SqlParameter> list);
        DataSet DeleteLine(List<SqlParameter> list);
    }
}
