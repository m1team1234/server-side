﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Interfaces
{
    public interface Igeometries
    {
        DataSet AddGeometry(List<SqlParameter> list);
        DataSet GetGeometries(List<SqlParameter> list);
        DataSet DeleteGeometry(List<SqlParameter> list);
    }
}
