﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Interfaces
{
   public interface Iuser
    {
        /// <summary>
        /// This function checks if there is user with this parameters.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>the id of the user,and -1 if not exist.</returns>
        DataSet GetUser(List<SqlParameter> list);
        /// <summary>
        /// This function adds user to the database.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>the id of the new user,and -1 if failed.</returns>
        DataSet Register(List<SqlParameter> list);
        /// <summary>
        /// This function delete an user from the database.
        /// (realy, it's only change the user to be not active.) 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>1 if Succeeded,and -1 if failed.</returns>
        void DeleteUser(List<SqlParameter> list);
        /// <summary>
        /// This function update the details of the user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>1 if Succeeded,and -1 if failed.</returns>
        void UpdateUser(List<SqlParameter> list);
    }
}
