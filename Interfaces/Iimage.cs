﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace Interfaces
{
   public interface Iimage
   {
        

        /// <summary>
        /// This function return all the images that this user can edit.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>All the details of this images, without the images themselves.</returns>
        List<DataSet> GetImagesDetailsPerUser(int userId);

        /// <summary>
        /// This function return a list of users that can edit this image.
        /// </summary>
        /// <param name="imageId"></param>
        /// <returns>All the users that can edit this images.</returns>
        List<DataSet> GetImageDetails(int imageId);

        /// <summary>
        /// This function return the image that this user asked for.
        /// </summary>
        /// <param name="imageId"></param>
        /// <returns>The requested image</returns>
        string GetImage(int imageId);

        /// <summary>
        /// This function gets an image with all the details, and add it to the database.
        /// </summary>
        /// <param name="image"></param>       
        DataSet AddImage(List<SqlParameter> list);

        /// <summary>
        /// This function gets an imageId and delete this image from the database.
        /// (realy, it's only change the image to be not active.)
        /// </summary>
        /// <param name="imageId"></param>
        void DeleteImage(int imageId);
    }
}
