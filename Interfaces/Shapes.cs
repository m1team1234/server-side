﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Interfaces
{
    interface Shapes
    {

        /// <summary>Return all the shapses that found in the database.(circle, rectangle, ect.)</summary>
        List<DataSet> GetShapes();

    }
}
