﻿using Idal;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{

    public class FileController : ApiController
    {
        [Route("api/File/SaveFile/{fileName}")]
        [HttpPost]
        public IHttpActionResult SaveFileByBase64(string fileName, [FromBody] d base64File)
        {
            string fileneme = FileBll.SaveFileByBase64(base64File.base64, fileName);
            return Ok(fileneme);
        }
        [Route("api/File/GetBase64StringForDocument/{documentName}")]
        [HttpGet]
        public string GetBase64StringForDocument(string documentName)
        {
            return FileBll.GetBase64StringForDocument(documentName);
        }
        [Route("api/File/savefileserver")]
        [HttpPut]

        public IHttpActionResult savefileserver( [FromBody] List<Idal.DTO.ImagesDTO> ImagesDTO)
        {
            List<SqlParameter> list = convert.convert<Idal.DTO.ImagesDTO>.convertTypeToSqlParameter(ImagesDTO);
            Iimage image = factory.factory.resolve<Iimage>();
            var db = image.AddImage(list);
            var convert2 = convert.convert<Idal.DTO.ImagesDTO>.convertDBsetToT(db);
            return Ok(convert2);
        }
    }
}
