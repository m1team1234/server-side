﻿using Idal;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("api/user")]

    public class userController : ApiController
    {
        [Route("findUser")]

        // POST: api/User
        public IHttpActionResult Put([FromBody]List<string> value)
        {
            List<SqlParameter> list = convert.convert<string>.convertTypeToSqlParameter(value);
            Iuser user = factory.factory.resolve<Iuser>();
            var db = user.GetUser(list);
            var convert2 = convert.convert<Idal.DTO.UserDTO>.convertDBsetToT(db);
            return Ok(convert2);
        }

        [Route("addUser")]
        public IHttpActionResult Post([FromBody]List<string> value)
        {
            List<SqlParameter> list = convert.convert<string>.convertTypeToSqlParameter(value);
            Iuser user = factory.factory.resolve<Iuser>();
            var db = user.Register(list);
            var convert2 = convert.convert<Idal.DTO.UserDTO>.convertDBsetToT(db);
            return Ok(convert2);
            
        }

    }
}
