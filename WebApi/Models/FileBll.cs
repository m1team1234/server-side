﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class FileBll
    {
        public static string SaveFileByBase64(string base64File, string fileName, string folderPath = "")
        {
            try
            {
                if (base64File.IndexOf(";base64,") != -1)
                {
                    // Create a new folder, if necessary.
                    if (!Directory.Exists(@AppDomain.CurrentDomain.BaseDirectory))
                        Directory.CreateDirectory(@AppDomain.CurrentDomain.BaseDirectory + ReadSetting("FileFolderPath") + folderPath);

                    if (fileName == null || fileName == "")
                        fileName = Guid.NewGuid() + "." + base64File.Substring(base64File.IndexOf('/') + 1, base64File.IndexOf(';') - (base64File.IndexOf('/') + 1));
                    else
                    {
                        int index = 1;
                        string newFilename = fileName;
                        while (File.Exists(@AppDomain.CurrentDomain.BaseDirectory + "filesnew\\" + newFilename))
                        {
                            newFilename = fileName.Split('.')[0] + "(" + index + ")" + (fileName.Split('.').Length > 1 ? "." + fileName.Split('.')[1] : "");
                            index++;
                        }
                        fileName = newFilename;
                    }

                    string sPath = AppDomain.CurrentDomain.BaseDirectory + "filesnew\\" + fileName;
                    byte[] array = Convert.FromBase64String(base64File.Substring(base64File.IndexOf(",") + 1));
                    File.WriteAllBytes(sPath, array);
                    
                    return fileName;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        private static string ReadSetting(string v)
        {
            throw new NotImplementedException();
        }
        public static string GetBase64StringForDocument(string documentName)
        {
            string documentPath = @AppDomain.CurrentDomain.BaseDirectory + "filesnew\\" + documentName + ".jpg";
            byte[] imageBytes = System.IO.File.ReadAllBytes(documentPath);
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }
}