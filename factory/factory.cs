﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace factory
{
    public class factory
    {
        private static Type[] mytypes { get; set; }
        private static Interfaces.Implement myAttributes { get; set; }
        private static Dictionary<Type, Type> FindClass { get; set; }
        public static void LoudFile()
        {
            FindClass = new Dictionary<Type, Type>();
            var arrFiles = Directory.GetFiles(@"D:\צורות\server-side\DAL\DAL\bin\Debug\", "*.dll");
            foreach (var assembly in arrFiles)
            {
                Assembly asm = Assembly.LoadFrom(assembly);
                mytypes = asm.GetTypes();
                foreach (var type in mytypes)
                {
                    if (type.IsClass)
                    {
                    
                        myAttributes = type.GetCustomAttribute<Interfaces.Implement>();
                    }
                    if (myAttributes != null)
                    {
                        FindClass.Add(myAttributes.Implementaion, type);

                    }

                }
            }
        }


        public static T resolve<T>() where T:class
        {
            var result = FindClass[typeof(T)];
            if (result != null)
            {
                //return result;
                return Activator.CreateInstance(FindClass[typeof(T)]) as T;
            }
            else
            {
                throw new Exception("No class was found to implement the requested interface");
            }

        }
        //or
        //public static T resolve<T>()
        //{
        //    var result = FindClass[typeof(T)];
        //    if (result != null)
        //    {
        //        return typeof(Activator.CreateInstance(result));
        //    }
        //    else
        //    {
        //        throw new Exception("No class was found to implement the requested interface");
        //    }

        //}

    }
}
