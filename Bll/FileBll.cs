﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FileServer.Models
{
    public class FileBll
    {
        /// <summary>
        /// שמירת הקובץ בתקיה פנימת
        /// </summary>
        /// <param name="base64File"></param>
        /// <param name="fileName"></param>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static string SaveFileByBase64(string base64File, string fileName, string folderPath = "")
        {
            try
            {
                if (base64File.IndexOf(";base64,") != -1)
                {
                    // Create a new folder, if necessary.
                    if (!Directory.Exists(@AppDomain.CurrentDomain.BaseDirectory))
                        Directory.CreateDirectory(@AppDomain.CurrentDomain.BaseDirectory + ReadSetting("FileFolderPath") + folderPath);

                    if (fileName == null || fileName == "")
                        fileName = Guid.NewGuid() + "." + base64File.Substring(base64File.IndexOf('/') + 1, base64File.IndexOf(';') - (base64File.IndexOf('/') + 1));
                    else
                    {
                        int index = 1;
                        string newFilename = fileName;
                        while (File.Exists(@AppDomain.CurrentDomain.BaseDirectory + "files1\\" + newFilename))
                        {
                            newFilename = fileName.Split('.')[0] + "(" + index + ")" + (fileName.Split('.').Length > 1 ? "." + fileName.Split('.')[1] : "");
                            index++;
                        }
                        fileName = newFilename;
                    }

                    string sPath = AppDomain.CurrentDomain.BaseDirectory  + "files1\\" + fileName;
                    byte[] array = Convert.FromBase64String(base64File.Substring(base64File.IndexOf(",") + 1));
                    File.WriteAllBytes(sPath, array);

                    return fileName;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        private static string ReadSetting(string v)
        {
            throw new NotImplementedException();
        }
        public static string GetBase64StringForDocument(string documentName)
        {
            string documentPath = @AppDomain.CurrentDomain.BaseDirectory + "files1\\" + documentName + ".jpg";
            byte[] imageBytes = System.IO.File.ReadAllBytes(documentPath);
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }
}