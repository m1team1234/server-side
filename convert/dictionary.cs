﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace convert
{
   public static class dictionary
    {
        
        public static Dictionary<Type, SqlDbType> DictionaryToSql { get; set; }
        public static Dictionary<SqlDbType,Type> DictionaryToType { get; set; }
        static dictionary()
        {
            DictionaryToSql = new Dictionary<Type, SqlDbType>()
            {
                {typeof(Int32),SqlDbType.Int },
                {typeof(string),SqlDbType.VarChar },
                {typeof(bool),SqlDbType.Bit },
                {typeof(DateTime),SqlDbType.Date },
                {typeof(float),SqlDbType.Float },
            };
            DictionaryToType = new Dictionary<SqlDbType, Type>()
            {
                { SqlDbType.VarChar,typeof(string) },
                { SqlDbType.Int,typeof(Int32) },
                { SqlDbType.Bit,typeof(bool) },
                {SqlDbType.Date,typeof(DateTime) },
                { SqlDbType.Float,typeof(float) },
            };
        }
    }
}
