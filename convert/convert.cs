﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace convert
{
    public class convert<T>
    {
        public static List<SqlParameter> convertTypeToSqlParameter(List<T> ObjectToConvert)
        {
            List<SqlParameter> parameter = new List<SqlParameter>();

            foreach (var item in ObjectToConvert)
            {
                if (!baseType.list.Contains(item.GetType()))
                {
                    PropertyInfo[] properties = item.GetType().GetProperties();
                    foreach (var prop in properties)
                    {
                        if (prop.GetCustomAttribute < Idal.DTO.IdentityAttribute >()== null)
                        {
                            SqlParameter parm = new SqlParameter();
                            parm.SqlDbType = dictionary.DictionaryToSql[prop.PropertyType];
                            parm.Value = prop.GetValue(item);
                            parm.ParameterName = "@" + prop.Name;
                            parameter.Add(parm);
                        }
                    }
                    
                }
                else
                {
                    SqlParameter parm = new SqlParameter();
                    parm.SqlDbType = dictionary.DictionaryToSql[item.GetType()];
                    parm.Value = item;
                    parameter.Add(parm);
                }

            }
            return parameter;

        }
        public static List<T> convertDBsetToT(DataSet dataset)
        {
            List<T> list = new List<T>();
            //if requested a class type
            if (dictionary.DictionaryToSql.ContainsKey(typeof(T)))
            {
                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    T Instance = Activator.CreateInstance<T>();
                    var properties = Instance.GetType().GetProperties();
                    foreach (var item in properties)
                    {
                        item.SetValue(Instance, row[item.Name]);
                    }
                    list.Add(Instance);
                }
            }
            else //requested a simple type
            {
                foreach (DataTable table in dataset.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        T Instance = Activator.CreateInstance<T>();
                        PropertyInfo[] properties = Instance.GetType().GetProperties(); 
                        for (int i = 0; i < row.ItemArray.Length; i++)
                        {
                            properties[i].SetValue(Instance, row.ItemArray[i]);
                        }
                        list.Add(Instance);


                    }
                }
            }
            return list;
        }
    }
}

//public List<SqlParameter> ConvertDTOToDBset(T data)
//{
//    Type typeT = typeof(T);
//    PropertyInfo[] properties = typeT.GetProperties();
//    List<SqlParameter> listParam = new List<SqlParameter>();
//    foreach (var prop in properties)
//    {
//        if ((prop.GetCustomAttribute<DTO.IdentityAttribute>() == null ||
//        (prop.GetCustomAttribute<DTO.IdentityAttribute>() != null && (int)prop.GetValue(data) != 0))
//        && prop.GetCustomAttribute<DTO.NotIncludeAttribute>() == null)
//        {
//            listParam.Add(new SqlParameter("@" + prop.Name, prop.GetValue(data)));
//        }
//    }
//    return listParam;
//}
