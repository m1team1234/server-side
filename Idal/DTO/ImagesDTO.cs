﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idal.DTO
{

   public class ImagesDTO
    {
        [IdentityAttribute]

        public int ImageId { get; set; }
        public int UserId { get; set; }
        public string image_name{ get; set; }
        public string comments{ get; set; }
        public string img { get; set; }
        public ImagesDTO()
        {

        }


    }
}
