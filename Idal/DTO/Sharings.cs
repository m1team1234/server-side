﻿using Idal.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DTO
{

   public class Sharings
    {
        [IdentityAttribute]
        public int ShringId { get; set; }
        public int ImageId { get; set; }
        public int UserId { get; set; }
    }

}
