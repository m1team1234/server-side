﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idal.DTO
{
    public class LinesDTO:DrawingsDTO
    {        
        [IdentityAttribute]
        public int LineId { get; set; }
        public float xStart { get; set; }
        public float yStart { get; set; }
        public float xEnd { get; set; }
        public float yEnd { get; set; }        
    }
}
