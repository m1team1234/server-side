﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
namespace Idal.DTO
{ 
   public class UserDTO
    {
        [IdentityAttribute]
        public int id { get; set; }
        public bool exists { get; set; }
        public string password { get; set; }
        public string userName { get; set; }

        public UserDTO()
        {

        }
        public UserDTO(int Id, bool Exisit)
        {
            this.id = Id;
            this.exists = Exisit;

        }
        public UserDTO(bool Exisit)
        {
            this.exists = Exisit;

        }
       
    }


}

