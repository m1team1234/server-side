﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idal.DTO
{
    
  public  abstract class DrawingsDTO
    {
        [IdentityAttribute]

        public int DrawingId { get; set; }
        public int ImageId { get; set; }
        public string LineColor { get; set; }
        public string DrawingName { get; set; }
        public string Comments { get; set; }
    }
}
